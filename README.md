# PA_5 Projektarbeit WebShop


## Wichtige Links

## Mitglieder
-   [Adrian Tang](https://moodle.ffhs.ch/user/view.php?id=9808&course=7929)
-   [Denojan Rishikeswaran](https://moodle.ffhs.ch/user/view.php?id=10680&course=7929)
-   [Paul Salomon](https://moodle.ffhs.ch/user/view.php?id=10736&course=7929)
## WebShop

Im Rahmen des Moduls [PA_5 Projektarbeit](https://moodle.ffhs.ch/course/view.php?id=7929) wird ein WebShop impleimplementiert. Das Front-End soll mithilfe des JavaScript-Framework [React](https://reactjs.org/) aufgebaute werden. Das Back-End wird mit Java realisiert.


## Semesterarbeit

Mit dem Projekt soll die interdisziplinäre Verbindung geschaffen werden, eine funktionsfähige Software-Lösung von Anfang bis Ende zu Planen. Dabei muss bei der Umsetzung adäquat auf die Anforderungen des Auftrages geachtet werden sowie eine aus dem Software-Umfeld gängige  Projektmanagement-Methoden gewählt werden um die Steureung und Kontrolle des Projektes zu gewährleisten. Auch soll das Projekt daszu beitragen, Lösungkonzepte für das gewählte Projekt zu erarbeiten und mit dem passenden Entwufs-Werkzeug Modelle und Architekturen zu kreieren.      

## Kriterien

Die nachfolgenden Punkte verschaffen einen groben Überblick über die Module, für die Anforderungen spezifiziert wurden sind. Diese Vorgaben und Anforderungen können im Detail __[hier](https://moodle.ffhs.ch/pluginfile.php/3948257/mod_resource/content/2/PA_DCS_Arbeitsmappe_HS20-21_V01_Social%20Media.pdf)__ nachgelesen werden. 

-   Vorgabe Projektmanagement
-   Vorgabe Social Media-Auftritt
-   Vorgabe Statusberichte
-   Vorgabe Abschlussbericht