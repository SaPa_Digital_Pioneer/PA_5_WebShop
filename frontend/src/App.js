import React from "react";
import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";

import {CartContextProvider} from './cart/Cart';
import {CartPage} from './cart-page/CartPage';
import {ProductPage} from "./product-page/ProductPage";
import {CartItemCount} from './cart-page/CartItemCount'
import {LandingPage} from "./landing-page/LandingPage";

function App() {
        return (
            <CartContextProvider>
                <Router>
                    <div className="App">
                        <nav
                            className="navbar container"
                            role="navigation"
                            aria-label="main navigation"
                        >
                            <div className="navbar-brand">
                                <b className="navbar-item is-size-4 "><Link to="/">FFHS Shop Projekt</Link></b>
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                                    <span aria-hidden="true"></span>
                            </div>
                            <div className={`navbar-menu `}>
                                <Link to="/products" className="navbar-item">
                                    Products
                                </Link>
                                <Link to="/cart" className="navbar-item">
                                    Cart
                                    <CartItemCount></CartItemCount>
                                </Link>
                            </div>
                        </nav>
                        <Switch>
                            <Route exact path="/" component={LandingPage} />
                            <Route exact path="/cart" component={CartPage} />
                            <Route exact path="/products" component={ProductPage} />
                        </Switch>
                    </div>
                </Router>
            </CartContextProvider>
        );
}
export default App;
