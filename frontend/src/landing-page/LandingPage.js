export const LandingPage = () => {
    return (
        <>
            <div className="hero is-light is-medium is-bold">
                <div className="hero-body container">
                    <h4 className="title">Landing Page</h4>
                </div>
            </div>
            <br />
            <div className="container has-text-centered">
                <div className="column is-6 is-offset-3">
                    <div className="hero-body">
                        <div className="container">
                            <div className="card">
                                <div className="card-content">
                                    <div className="content">
                                        <div className="control has-icons-left has-icons-right">
                                            <input className="input is-large" type="search"/>
                                            <span className="icon is-medium is-left">
                            <i className="fas fa-map-marker"></i>
                        </span>
                                            <span className="icon is-medium is-right">
                            <i className="fas fa-search"></i>
                        </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
        );
    }


export default LandingPage;
