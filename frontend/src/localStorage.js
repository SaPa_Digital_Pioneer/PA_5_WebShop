export const loadCartState = () => {
    let cart = localStorage.getItem("cart");
    cart = cart? JSON.parse(cart) : {};
    return cart;
}

export const saveCartState = (cart) => {
    try {
        cart = cart ? JSON.parse(cart) : {};
        localStorage.setItem("cart", JSON.stringify(cart));
    }catch{
        return undefined;
    }
}
