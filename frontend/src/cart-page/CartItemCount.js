import {GetCartCount} from "../cart/state/cartActions";

export const CartItemCount = () => {
    return(
        <span
            className="tag is-primary"
            style={{ marginLeft: "5px" }}
        >
            <GetCartCount></GetCartCount>
                  </span>
    )
}
