import React, {useState, useEffect, createContext, useLayoutEffect, useReducer, useContext} from "react";
import axios from "axios";
import {ProductReducer} from "./state/productReducer";
import ProductItem from "./ProductItem";
import {getProducts} from "../api/ProductAPI";

export const ProductContext = createContext({});

export const ProductContextProvider = (props) => {

    const reducer = ProductReducer;
    const initialState = {
        products: {}
    };

    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        getProducts().then( data => {
            dispatch({
                type: "FETCH_PRODUCTS",
                products: data
            });
        })

    },[]);

    return (
      <ProductContext.Provider value={{state , dispatch}}>
          {props.children}
      </ProductContext.Provider>
    );
}

export const Products = () => {
    const {state, dispatch } = useContext(ProductContext);
    return (<>
            <div className="hero is-light is-medium is-bold">
                <div className="hero-body container">
                    <h4 className="title">Our Products</h4>
                </div>
            </div>
            <br />
            <div className="container">
                <div className="column columns is-multiline">
                    {state.products && state.products.length ? (
                        state.products.map((product, index) => (
                            <ProductItem
                                product={product}
                                key={index}
                            />
                        ))
                    ) : (
                        <div className="column">
              <span className="title has-text-grey-light">
                loading...
              </span>
                        </div>
                    )}
                </div>
            </div>
        </>
    );
};
