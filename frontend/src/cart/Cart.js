import React, {useEffect, createContext, useContext, useReducer} from "react";
import CartItem from "./CartItem";
import {CartReducer} from "./state/cartReducer";

export const CartContext = createContext({});

export const CartContextProvider = (props) => {
    const reducer = CartReducer;
    const initialState = {
        cart:  localStorage.getItem("cart")? JSON.parse(localStorage.getItem("cart")) : {}
    };

    const [state, dispatch] = useReducer(reducer, initialState);

    useEffect(() => {
        let cart = localStorage.getItem("cart");
        cart = cart? JSON.parse(cart) : {};
        localStorage.setItem("cart", JSON.stringify(cart));
    },[state]);


    return (
        <CartContext.Provider value={{state, dispatch}}>
            {props.children}
        </CartContext.Provider>
    )
}

export const Cart = () => {
    const {state} = useContext(CartContext);
    const cartKeys = Object.keys(state.cart || {});

    return (
        <>
                <div className="hero is-light is-bold is-medium">
                    <div className="hero-body container">
                        <h4 className="title">My Cart</h4>
                    </div>
                </div>
                <br />
                <div className="container">
                    {cartKeys.length ? (
                        <div className="column columns is-multiline">
                            {cartKeys.map(key => (
                                <CartItem
                                    cartKey={key}
                                    key={key}
                                    cartItem={state.cart[key]}
                                />
                            ))}
                            <div className="column is-12 is-clearfix">
                                <br />
                                <div className="is-pulled-right">
                                    <button

                                        className="button is-warning "
                                    >
                                        Clear cart
                                    </button>{" "}
                                    <button
                                        className="button is-success"

                                    >
                                        Checkout
                                    </button>
                                </div>
                            </div>
                        </div>
                    ) : (
                        <div className="column">
                            <div className="title has-text-grey-light">No item in cart!</div>
                        </div>
                    )}
                </div>
        </>
    );
}
