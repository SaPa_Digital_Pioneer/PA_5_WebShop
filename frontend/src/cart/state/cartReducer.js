export const CartReducer = (state, action) => {
    switch (action.type) {
        case "FETCH_ITEMS":
            return {
                ...state
            };

        case "ADD_ITEM":
            let cart = state.cart;
            if (cart[action.cartItem.id]){
                cart[action.cartItem.id].amount += 1;
            }else{
                cart[action.cartItem.id] = action.cartItem;
                cart[action.cartItem.id].amount = 1;
            }
            localStorage.setItem("cart", JSON.stringify(cart));
            return {
                ...state
            };

        case "REMOVE_ITEM":
            let cartRemove = state.cart;
            delete cartRemove[action.cartItemId];
            localStorage.setItem("cart", JSON.stringify(cartRemove));
            return {
                ...state
            };
        default:
            return state;
    }
};
