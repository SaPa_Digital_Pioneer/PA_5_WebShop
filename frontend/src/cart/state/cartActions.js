import React, {useContext} from "react";
import {CartContext} from "../Cart";

export const AddToCart = props => {
    const {dispatch } = React.useContext(CartContext);
    const addToCart = cartItem => {
        dispatch({
            type: "ADD_ITEM",
            cartItem: cartItem
        });
    }

    return (
        <button
            className="button is-small is-outlined is-primary   is-pulled-right"
            onClick={e => addToCart(props.product)}
        >
            Add to Cart
        </button>
    );
};

export const RemoveFromCart = (props) => {
    const {dispatch} = useContext(CartContext);
    const onClick = cartItemId => {
        dispatch({
            type: "REMOVE_ITEM",
            cartItemId: cartItemId,
        });
    }

    return (
        <div
            className="media-right"
            onClick={() => onClick(props.cartKey)}
        >
            <span className="delete is-large"></span>
        </div>
    );
};

export const GetCartCount = () => {
    const { state} = useContext(CartContext);
    return(
        <>
            {Object.keys(state.cart).length}
                  </>
    )
}
