import React from "react";
import {ProductContextProvider, Products} from "../product/Product";

export const ProductPage = () => {

    return(
        <ProductContextProvider>
            <Products></Products>
        </ProductContextProvider>
    )
}
